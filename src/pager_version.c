//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "pager.h"
#include "pager_data.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
// Identification
#define PAGER_PACKAGE			"pager library"
#ifdef VERSION
#define PAGER_VERSION(x) 		str(x)
#define str(x)					#x
#else
#define PAGER_VERSION 			"Unknown"
#endif
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
void pager_version( char *version, size_t len ) {
	snprintf(version, len, "%s - Version %s", PAGER_PACKAGE, PAGER_VERSION(VERSION));
}
//------------------------------------------------------------------------------
