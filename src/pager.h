//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------
#ifndef PAGER_H
#define PAGER_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stddef.h>
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS DEFINITIONS
//------------------------------------------------------------------------------
typedef enum { SYNCHRONOUS, ASYNCHRONOUS } epager_view;
typedef enum { REDRAW_ON_RESIZING, STOP_ON_RESIZING } epager_resizing;
//------------------------------------------------------------------------------
// STRUCTURE
//------------------------------------------------------------------------------
struct sPager {
	char			*filename;			// Name of file to view
	char			*header;			// Text that will be displayed at the head of each page
	char			*endbanner;			// Text to add to the message displayed at the end of the file
	char			*prefixerror;		// Text to add at the beginning of the error message
	epager_view		viewtype;			// Type of the visualization process
	epager_resizing	resizingtype;		// Action to do when the window is resized
};
//------------------------------------------------------------------------------
// ROUTINES DEFINITIONS
//------------------------------------------------------------------------------
extern int pager_view( struct sPager );
extern int pager_wait( void );
extern void pager_version( char *, size_t );
extern void pager_cleaner( void );
//------------------------------------------------------------------------------
#endif	// PAGER_H
