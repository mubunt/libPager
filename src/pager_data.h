//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------
#ifndef PAGER_DATA_H
#define PAGER_DATA_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <termcap.h>
#include <ctype.h>
#include <wchar.h>
#include <locale.h>
#include <setjmp.h>
#include <semaphore.h>
#include <stdbool.h>
#include <errno.h>
#include <stddef.h>
#include <sys/stat.h>
#include <sys/types.h>
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define pager_FOREGROUND_1						"\033[1;30m"	// BLACK
#define pager_FOREGROUND_2						"\033[1;31m"	// RED
#define pager_FOREGROUND_2_W					L"\033[1;31m"	// RED
#define pager_FOREGROUND_3						"\033[0;33m"	// YELLOW
#define pager_FOREGROUND_4						"\033[1;37m"	// WHITE
#define pager_BACKGROUND_1						"\033[1;41m"	// RED
#define pager_BACKGROUND_2						"\033[1;47m"	// WHITE
#define pager_BACKGROUND_3						"\033[1;47m"	// WHITE
#define pager_STOP_COLOR						"\033[0m"
#define pager_STOP_COLOR_W						L"\033[0m"

#define	pager_XSH								"─"
#define pager_XSV								"│"
#define pager_XST 								"┼"
#define pager_XSTR								"├"

#define BS 										0x08
#define RETURN 									0x0a
#define CR 										0x0d
#define SPACE									0x20
#define ESCAPE									0x1b
#define ARROW_UP								0x41
#define ARROW_DOWN								0x42
#define ARROW_RIGHT								0x43
#define ARROW_LEFT								0x44
#define OPENBRACKET								0x5b
#define DEL 									0x7f

#define DISPLAY(...)							fprintf(stdout,  __VA_ARGS__)
#define FLUSH()									fflush(stdout)

#define SAVEINITSCREEN(n)						tputs(vpager_screeninit, (int)n, putchar)
#define RESTOREINITSCREEN(n)					tputs(vpager_screendeinit, (int)n, putchar)
#define CLEAR_ENTIRE_SCREEN()					tputs(vpager_clear, (int)vpager_nbLINES, putchar)
#define MOVE_CURSOR(x, y)						tputs(tgoto(vpager_move, (int)(y - 1), (int)(x - 1)), 1, putchar)
//#define DISPLAY_BELL()						tputs(vpager_visual_bell, vpager_nbLINES, putchar);

// To migrate to portable mechanism (terminfo, tputs, ...) later!!!
#define MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(x, y)	DISPLAY("\033[%ld;%ldH\033[K", x, y)
#define HIDE_CURSOR()							DISPLAY("\033[?25l")
#define SHOW_CURSOR()							DISPLAY("\033[?25h")
#define SAVE_CURSOR_POSITION()					DISPLAY("\033[s")
#define RESTORE_CURSOR_POSITION()				DISPLAY("\033[u")
#define CLEAR_CURRENT_LINE()					DISPLAY("\033[K")

#define DISPLAY_LINE(n)							for (int i = 0; i < n; i++) DISPLAY("%s", pager_XSH);
#define DISPLAY_TOPLINE()						for (int i = 0; i < 6; i++) DISPLAY("%s", pager_XSH); \
												DISPLAY("%s", pager_XST); \
												for (size_t i = 7; i < vpager_nbCOLS; i++) DISPLAY("%s", pager_XSH);
#define DISPLAY_HEADERLINE()					for (int i = 0; i < 6; i++) DISPLAY(" "); \
												DISPLAY("%s", pager_XSTR); \
												for (size_t i = 7; i < vpager_nbCOLS; i++) DISPLAY("%s", pager_XSH);
#define DISPLAY_BOTTOMLINE(buf1, buf2)			MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(vpager_nbLINES, (size_t)1); \
												DISPLAY("%s%s%s", pager_FOREGROUND_1, pager_BACKGROUND_2, buf1); \
												for (size_t i = strlen(buf1) + strlen(buf2); i < (size_t) vpager_nbCOLS; i++) DISPLAY(" "); \
												DISPLAY("%s%s", buf2, pager_STOP_COLOR)
#define DISPLAY_BOTTOMMESSAGE(msg)				MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(vpager_nbLINES, (size_t)1); \
												DISPLAY("%s%s%s", pager_FOREGROUND_1, pager_BACKGROUND_2, msg); \
												for (size_t i = strlen(msg); i < (size_t) vpager_nbCOLS; i++) DISPLAY(" "); \
												DISPLAY("%s", pager_STOP_COLOR)
#define DISPLAY_MESSAGE(msg)					MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(vpager_nbLINES, (size_t)1); \
												DISPLAY("%s%s%s%s", pager_FOREGROUND_4, pager_BACKGROUND_1, msg, pager_STOP_COLOR)
#define DISPLAY_BELL()							DISPLAY("\a"); FLUSH()
#define max(a,b)								a > b ? a : b;
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS DEFINITIONS
//------------------------------------------------------------------------------
typedef enum { MODE_REGULAR, MODE_OVERLAP } epager_display;
typedef enum { MODE_ECHOED, MODE_RAW } epager_terminal;
typedef enum { PAGE_FRESHLY_DISPLAYED, ALREADY_SCROLLED_UP, ALREADY_SCROLLED_DOWN } epager_action;
typedef enum { NDISPLAY, NCLEAR } epager_pending;
typedef enum { NOP, FORWARD, BACKWARD } epager_search;
typedef enum { TODISPLAY, TOSEARCH, TOHIGHLIGHT } epager_page;
//------------------------------------------------------------------------------
// STRUCTURE DEFINITIONS
//------------------------------------------------------------------------------
struct spager_offset {
	off_t offset;						// offset of the page
	off_t offset_start_plus_OVERLAP;	// offset of the fifth line after the start of the page
	off_t offset_end_minus_OVERLAP;		// offset of the fifth line before the end of the page
	size_t line;						// Number of the first line of the page
};
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern size_t 			vpager_nbLINES;
extern size_t 			vpager_nbCOLS;
extern size_t 			vpager_nblines;
extern size_t 			vpager_headerlines;
extern char 			**vpager_header;
extern epager_view		vpager_asynchronousmode;
extern pthread_t 		vpager_viewthread;
extern pthread_t 		vpager_sizemonitoring;
extern char 			*vpager_screeninit;		// Startup terminal initialization
extern char 			*vpager_screendeinit;	// Exit terminal de-initialization
extern char 			*vpager_clear;			// Clear screen
extern char 			*vpager_move;			// Cursor positioning
extern char				*vpager_prefixerror;	// Prefix for error message
extern bool				vpager_waitacknowledge;	// Wait or not an user's acknolegge on error
//extern char * 			vpager_visual_bell;		// Visual bell (flash screen) sequence
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern void				pager_help( void );
extern void 			pager_info( char * );
extern void				pager_sleep_ms( int );
extern void				pager_error( const char *, ... );
extern void				pager_pendingnumber( epager_pending, size_t );
extern void				pager_pendingpattern(epager_pending, char * );
extern void 			pager_freeheader( void );
extern void 			pager_setoffset( FILE *, off_t );
extern off_t 			pager_getoffset( FILE * );
extern off_t 			pager_getsize( char * );
extern int 				pager_isatextfile( char * );
extern int 				pager_incoming( size_t *, char *, size_t );
extern bool 			pager_filecheck( char * );
extern bool 			pager_existfile( char * );
extern bool				pager_generateheader( char * );
extern bool				pager_GetTerminalSize( size_t *, size_t * );
//------------------------------------------------------------------------------
#endif	// PAGER_DATA_H
