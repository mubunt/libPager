//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "pager.h"
#include "pager_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static const char vpager_help[][63] = {
	" q, Q                     Quit                              ",
	" h, H                     This help                         ",
	" <enter>, <space>, ▶      Next page                         ",
	" p, P, ◀                  Previous page                     ",
	" <n><enter>, <space>, ▶   Goto page <n>                     ",
	" e, E                     Goto end                          ",
	" ▼ (once only)            Five next lines                   ",
	" ▲ (once only)            Five previous line                ",
	" r, R                     Refresh screen                    ",
	" i, I                     Information about file            ",
	" /pattern/                Search forward for matching line  ",
	" ?pattern?                Search backward for matching line ",
	" n, N                     Repeat previous search            "
};

static const char vpager_info[][28] = {
	" Newline counts .......... ",
	" Character counts ........ ",
	" Maximum display width ... "
};
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static void _pager_SetTerminalMode(epager_terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		tcsetattr(STDIN_FILENO, TCSANOW, &cooked);
		break;
	case MODE_RAW:
		tcgetattr(STDIN_FILENO, &cooked);
		raw = cooked;
		raw.c_lflag &= (tcflag_t) ~(ICANON | ECHO);
		raw.c_cc[VMIN] = 1;
		raw.c_cc[VTIME] = 0;
		tcsetattr(STDIN_FILENO, TCSANOW, &raw);
		break;
	}
}
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
bool pager_GetTerminalSize( size_t *lines, size_t *cols ) {
	struct winsize w;
	// Warning: STDIN_FILENO and NOT STDOUT_FILENO !!!
	if (0 != ioctl(STDIN_FILENO, TIOCGWINSZ, &w)) return false;
	*lines = w.ws_row;
	*cols = w.ws_col;
	return true;
}

void pager_sleep_ms( int milliseconds ) {
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
}

void pager_error( const char *format, ... ) {
	va_list argp;
	char buff[256];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "%s%s %s%s\n", pager_FOREGROUND_2, vpager_prefixerror, buff, pager_STOP_COLOR);

	if (vpager_waitacknowledge) {
		SAVE_CURSOR_POSITION();
		DISPLAY("%s%s[press RETURN]%s", pager_FOREGROUND_1, pager_BACKGROUND_2, pager_STOP_COLOR);
		FLUSH();
		_pager_SetTerminalMode(MODE_RAW);
		while (RETURN != getchar());
		_pager_SetTerminalMode(MODE_ECHOED);
		RESTORE_CURSOR_POSITION();
		CLEAR_CURRENT_LINE();
		DISPLAY("%c", '\n');
		FLUSH();
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	HELP
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void pager_help( void ) {
#define NOHELP 	"Not enough space to display help!!!"
	size_t n = sizeof(vpager_help) / strlen(vpager_help[0]);
	size_t col = vpager_nbCOLS - strlen(vpager_help[0]) + 1;
	if (col <= 0 || n > vpager_nblines - 2) {	// 2 == header and trailer lines
		DISPLAY_MESSAGE((char *)NOHELP);
	} else {
		MOVE_CURSOR(vpager_nbLINES - n - 2, col);
		DISPLAY_LINE((int)(strlen(vpager_help[0])));
		for (size_t i = 0; i < n; i++) {
			MOVE_CURSOR(vpager_nbLINES - n - 1 + i, col);
			DISPLAY("%s%s%s%s", pager_FOREGROUND_1, pager_BACKGROUND_2, vpager_help[i], pager_STOP_COLOR);
		}
		MOVE_CURSOR(vpager_nbLINES - 1, col);
		DISPLAY_LINE((int)(strlen(vpager_help[0])));
	}
	FLUSH();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	INFORMATION ABOUT FILE
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static long int get_popen_data(char *command) {
	FILE *pf;
	char data[512];
	long int count;

	if (! (pf = popen(command,"r"))) return(-1);
	if (NULL == fgets(data, 512, pf)) return(-1);
	if (data[strlen(data) - 1] == '\n') data[strlen(data) - 1] = '\0';
	count = atoi(data);
	if (pclose(pf) == -1) return(-1);
	return(count);
}

void pager_info( char *file ) {
#define NOINFO	"Not enough space to display inforamtion about file!!!"
#define ERINFO	"Failed to run external command!!!"
	char command[1024];
	char opt[3] = "lcL";	// Line, Char, Mmax width
	size_t k = 9;			// Number of chars displayed for number (8, see hereafter) + 1
	size_t n = sizeof(vpager_info) / strlen(vpager_info[0]);
	size_t col = vpager_nbCOLS - strlen(vpager_info[0]) + 1 - k;
	if (col <= 0 || n > vpager_nblines - 2) {	// 2 == header and trailer lines
		DISPLAY_MESSAGE((char *)NOINFO);
	} else {
		for (size_t i = 0; i < n; i++) {
			sprintf(command, "wc -%c %s 2>&1", opt[i], file);
			long int r = get_popen_data(command);
			if (r < 0) {
				DISPLAY_MESSAGE((char *)ERINFO);
			} else {
				MOVE_CURSOR(vpager_nbLINES - n - 1 + i, col);
				DISPLAY("%s%s%s%08ld %s", pager_FOREGROUND_1, pager_BACKGROUND_3, vpager_info[i], r, pager_STOP_COLOR);
			}
		}
	}
	FLUSH();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	FILE MANAGEMENT
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
off_t pager_getoffset( FILE *fd ) {
	return(ftell(fd));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void pager_setoffset( FILE *fd, off_t offset) {
	fseek(fd, offset, SEEK_SET);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
off_t pager_getsize( char *file ) {
	struct stat st;
	stat(file, &st);
	return(st.st_size);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool pager_existfile( char *file ) {
	struct stat locstat;
	if (file == NULL) return false;
	if (stat(file, &locstat) < 0) return false;
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Make sure we are dealing with ascii text before working on
// 0 ... Text file
// 1 ... Binary (not-text) file
// 2 ... Cannot open
// 3 ... Cannot read => errno
int pager_isatextfile( char *file ) {
	char command[128], data[128];
	FILE *pf;

	if (! pager_existfile(file)) return 2;
	sprintf(command, "file -b %s", file);
	if (! (pf = popen(command,"r"))) return 3;
	char *p = fgets(data, 128, pf);
	fclose(pf);
	if (p == NULL) return 3;
	if (strstr(data, "text") == NULL)
		return 1;
	else
		return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool pager_filecheck( char *file ) {
	if (! pager_existfile(file)) {
		pager_error("File %s does not exist. Abort!", file);
		return false;
	}
	int n = pager_isatextfile(file);
	if (n == 0)
		return true;
	switch (n) {
	case 1:
		pager_error("File %s is not a text file. Abort!", file);
		break;
	case 2:
		pager_error("Cannot open %s file. Abort!", file);
		break;
	default:
		//pager_error("Cannot read %s file. Abort!", file);
		pager_error("%s. Abort!", strerror(n - 3));
		break;
	}
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	INPUT MANAGEMENT
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Reading  command:
// 0 ... Quit
// 1 ... Help
// 2 ... Next page
// 3 ... Previouis page
// 4 ... Goto page
// 5 ... 5 next lines
// 6 ... 5 previous lines
// 7 ... Refresh
// 8 ... Goto end
// 9 ... Information
// 10 .. Search forward
// 11 .. Search backward
// 12 .. Repeat previous search
// -1 .. Error
int pager_incoming(size_t *pagenumber, char *pattern, size_t length) {
	int c;
	size_t n = 0;
	char *ptpattern = pattern;
	*pagenumber = 0;
	switch ((c = getchar())) {
	case 'q':
	case 'Q':
		c = 0;
		break;
	case 'h':
	case 'H':
		c = 1;
		break;
	case CR:
	case RETURN:
	case SPACE:
		c = 2;
		break;
	case 'p':
	case 'P':
		c = 3;
		break;
	case 'r':
	case 'R':
		c = 7;
		break;
	case 'e':
	case 'E':
		c = 8;
		break;
	case 'i':
	case 'I':
		c = 9;
		break;
	case 'n':
	case 'N':
		c = 12;
		break;
	case ESCAPE:
		switch (getchar()) {
		case OPENBRACKET:
			c = getchar();
			switch (c) {
			case ARROW_RIGHT:
				c = 2;
				break;
			case ARROW_LEFT:
				c = 3;
				break;
			case ARROW_DOWN:
				c = 5;
				break;
			case ARROW_UP:
				c = 6;
				break;
			default:
				c = -1;
				break;
			}
			break;
		default:
			c = -1;
			break;
		}
		break;
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		n = c & 0xf;
		pager_pendingnumber(NDISPLAY, n);
		c = getchar();
		if (c == BS || c == DEL) {
			c = -1;
		} else {
			while ((c >= 0x30 && c <= 0x39) || c == BS || c == DEL) {
				if (c == BS || c == DEL) {
					n /= 10;
					if (n == 0) break;
				} else {
					n = n * 10 + (c & 0xf);
				}
				pager_pendingnumber(NDISPLAY, n);
				c = getchar();
			}
			if (n == 0) {
				c = -1;
			} else {
				*pagenumber = n;
				switch (c) {
				case CR:
				case RETURN:
				case SPACE:
					c = 4;
					break;
				case ESCAPE:
					switch (getchar()) {
					case OPENBRACKET:
						c = getchar();
						switch (c) {
						case ARROW_RIGHT:
							c = 4;
							break;
						default:
							c = -1;
							break;
						}
						break;
					default:
						c = -1;
						break;
					}
					break;
				default:
					c = -1;
					break;
				}
			}
		}
		pager_pendingnumber(NCLEAR, *pagenumber);
		break;
	case '/':
		c = getchar();
		if (c == BS || c == DEL) {
			c = -1;
		} else {
			while (c != '/' && c != CR) {
				if (c == BS || c == DEL) {
					--ptpattern;
					*ptpattern = '\0';
				} else {
					*ptpattern = (char)c;
					++ptpattern;
					*ptpattern = '\0';
				}
				pager_pendingpattern(NDISPLAY, pattern);
				if (strlen(pattern) == (length - 1)) break;
				c = getchar();
			}
			pager_pendingpattern(NCLEAR, pattern);
			c = 10;
		}
		break;
	case '?':
		c = getchar();
		if (c == BS || c == DEL) {
			c = -1;
		} else {
			while (c != '?' && c != CR) {
				if (c == BS || c == DEL) {
					--ptpattern;
					*ptpattern = '\0';
				} else {
					*ptpattern = (char)c;
					++ptpattern;
					*ptpattern = '\0';
				}
				pager_pendingpattern(NDISPLAY, pattern);
				if (strlen(pattern) == (length - 1)) break;
				c = getchar();
			}
			pager_pendingpattern(NCLEAR, pattern);
			c = 11;
		}
		break;
	default:
		c = -1;
		break;
	}
	return(c);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	HEADER GENERATION
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool pager_generateheader( char *header ) {
	vpager_headerlines = 0;
	vpager_header = NULL;
	if (strlen(header) == 0) return true;
	// Pass 1: Get local copy of header
	char *ptheader;
	if (NULL == (ptheader = malloc((strlen(header) + 2) * sizeof(char)))) return false;
	sprintf(ptheader, "%s\n", header);
	// Pass 2: Compute number of lines of the header
	for (size_t i = 0; i < strlen(ptheader); ++i) {
		if (ptheader[i] == '\n') ++vpager_headerlines;
	}
	// Pass 3: Allocate pointer array for generated header
	if (NULL == (vpager_header = malloc(((size_t)vpager_headerlines + 1) * sizeof(char *)))) return false;
	// Pass 4; Allocate each line and generate the final header
	int i = 0;
	char *pt, *ptp;
	ptp = pt = ptheader;
	while (*pt != '\0') {
		if (*pt == '\n') {
			*pt = '\0';
			vpager_header[i] = malloc((strlen(ptp) + 1) * sizeof(char));
			strcpy(vpager_header[i], ptp);
			++i;
			ptp = pt + 1;
		}
		++pt;
	}
	// Pass 5: Free local copy
	free(ptheader);
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void pager_freeheader( void ) {
	if (vpager_header != NULL) {
		for (size_t i = 0; i < vpager_headerlines; i++) {
			if (vpager_header[i] != NULL) free(vpager_header[i]);
			vpager_header[i] = NULL;
		}
		free(vpager_header);
		vpager_header = NULL;
	}
}
//------------------------------------------------------------------------------

