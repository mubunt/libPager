//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "pager.h"
#include "pager_data.h"
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
int pager_wait( void ) {
	// Check id current mode is asynchronous
	if (vpager_asynchronousmode != ASYNCHRONOUS) {
		pager_error("%s", "Unconsistent call to 'wait' routine. Abort!");
		return -1;
	}
	// Waiting thread displaying the file
	void  *tret;
	int n = pthread_join(vpager_viewthread, &tret);
	// Stop the size monitoring thread
	pthread_cancel(vpager_sizemonitoring);
	// Free local copy of header
	pager_freeheader();
	return n;
}
//------------------------------------------------------------------------------
