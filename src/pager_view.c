//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------f-----------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "pager.h"
#include "pager_data.h"
//------------------------------------------------------------------------------
// LOCAL CONSTANTS
//------------------------------------------------------------------------------
#define OVERLAP				5			// 5 lines
#define BUFSIZE				4096		// Size of file reading buffer
#define PREFIX_HEADER 		"      │ "	// Prefix of each header line.
#define NOPATTERN			(char *)"deadbeef"
#define EXIT_FAIL 			-1
#define EXIT_RESIZE			-2
#define EXIT_INTERRUPT		-3
//------------------------------------------------------------------------------
#define MEMSET_VPAGER_OFFSET()	memset((void *)vpager_offsets, 0, vpager_offsetssize * sizeof(struct spager_offset))

#define EOF_OF_FILE(n)			(_pager_GetPageOffset(n) == vpager_filesize)
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
epager_view 				vpager_asynchronousmode		= SYNCHRONOUS;
epager_resizing				vpager_resizing				= REDRAW_ON_RESIZING;
pthread_t 					vpager_viewthread			= 0;	// Id of the thread (asynchronous mode)
pthread_t 					vpager_sizemonitoring		= 0;	// Id of the file size monitoring thread
size_t 						vpager_nbLINES 				= 0;	// Number of lines of the terminal
size_t 						vpager_nbCOLS 				= 0;	// Number of columns of the terminal
size_t 						vpager_nblines 				= 0;	// Number of lines to display on the screen
size_t 						vpager_headerlines			= 0;	// Number line of user's header
char 						**vpager_header				= NULL;	// User's header
char 						*vpager_screeninit;					// Startup terminal initialization
char 						*vpager_screendeinit;				// Exit terminal de-initialization
char 						*vpager_clear;						// Clear screen
char 						*vpager_move;						// Cursor positioning
char						*vpager_prefixerror;				// Prefix for error message
bool						vpager_waitacknowledge		= true;	// Wait or not an user's acknolegge on error
//char * 					vpager_visual_bell;					// Visual bell (flash screen) sequence
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static size_t 				vpager_firstline			= 0;	// First line for user's data
static size_t 				vpager_lastline				= 0;	// Last line for user's data
static off_t 				vpager_filesize				= 0;	// Size if the file (under monitoring)
static size_t 				vpager_offsetssize			= 0;	// How many pages managed for the file
static size_t 				vpager_currentpage 			= 0;	// Current page number
static size_t 				vpager_lastpageRead			= 0;	// High page number displayed
static bool					vpager_lastPageRequested	= false;
static char 				*vpager_filename			= NULL;
static FILE 				*vpager_fd 					= NULL;
static struct spager_offset *vpager_offsets 			= NULL;	// Offset table
static epager_action		vpager_state 				= PAGE_FRESHLY_DISPLAYED;
static epager_display		vpager_currentmode			= MODE_REGULAR;
static pthread_mutex_t 		vpager_mutex_screen			= PTHREAD_MUTEX_INITIALIZER;
static jmp_buf				vpager_env_buffer;
static char					*vpager_additional_endbanner;	// Additional message for end banner
//static char					vpager_additional_endbanner[256];	// Additional message for end banner
//------------------------------------------------------------------------------
// INTERNAL ROUTINES / PREDEFINITION
//------------------------------------------------------------------------------
static void		_pager_ResetContext( void );
static void 	*_pager_SizemonitoringThread( void * );
static void 	_pager_DisplayTopLines( void );
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	OFFSET TABLE MANAGEMENT
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static off_t _pager_GetPageOffset( size_t n ) {
	return vpager_offsets[n].offset;
}

static void _pager_SetPageOffset( size_t n, off_t val ) {
	vpager_offsets[n].offset = val;
}

static off_t _pager_GetPageOffsetStart( size_t n ) {
	return vpager_offsets[n].offset_start_plus_OVERLAP;
}

static void _pager_SetPageOffsetStart( size_t n, off_t val ) {
	vpager_offsets[n].offset_start_plus_OVERLAP = val;
}

static off_t _pager_GetPageOffsetEnd( size_t n ) {
	return vpager_offsets[n].offset_end_minus_OVERLAP;
}

static void _pager_SetPageOffsetEnd( size_t n, off_t val ) {
	vpager_offsets[n].offset_end_minus_OVERLAP = val;
}

static size_t _pager_GetPageLine( size_t n ) {
	return vpager_offsets[n].line;
}

static void _pager_SetPageLine( size_t n, size_t val ) {
	vpager_offsets[n].line = val;
}

static bool _pager_AllocPageOffsetsTable( void ) {
#define OFFSET_UNIT		512
	struct spager_offset *save = vpager_offsets;
	vpager_offsetssize += OFFSET_UNIT;
	if (NULL == (vpager_offsets = (struct spager_offset *)calloc(vpager_offsetssize, sizeof(struct spager_offset)))) {
		pager_error("%s", "Cannot allocate memory for file offset management. Abort!");
		siglongjmp(vpager_env_buffer, EXIT_FAILURE);
	}
	MEMSET_VPAGER_OFFSET();
	if (save != NULL) {
		for (size_t i = 0; i < vpager_offsetssize - OFFSET_UNIT; i++) {
			_pager_SetPageOffset(i, save[i].offset);
			_pager_SetPageOffsetStart(i, save[i].offset_start_plus_OVERLAP);
			_pager_SetPageOffsetEnd(i, save[i].offset_end_minus_OVERLAP);
			_pager_SetPageLine(i, save[i].line);
		}
		free(save);
	}
	return true;
}

static void _pager_FreePageOffsetsTable( void ) {
	if (vpager_offsets != NULL) free(vpager_offsets);
	vpager_offsets = NULL;
}

static void _pager_NewPageAllocation( size_t page, size_t lines ) {
	_pager_SetPageOffset(page + 1, pager_getoffset(vpager_fd));
	_pager_SetPageLine(page + 1, _pager_GetPageLine(page) + lines);
	if (page >= vpager_lastpageRead) {
		vpager_lastpageRead = page + 1;
		vpager_lastPageRequested = (vpager_filesize == _pager_GetPageOffset(vpager_lastpageRead));
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	TERMINAL MANAGEMENT
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void pager_pendingnumber(epager_pending state, size_t c ) {
#define LABEL	"Pending number: "
	char s[16] = "";
	snprintf(s, sizeof(s), "%ld", c);
	size_t origin = vpager_nbCOLS - strlen(LABEL) - strlen(s) - 1;
	if (origin <= 0) origin = 1;
	MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE((size_t)1, origin - 1);
	MOVE_CURSOR(1, origin);
	DISPLAY("%s%s%s%s   ", pager_FOREGROUND_3, LABEL, s, pager_STOP_COLOR);
	if (state == NCLEAR) {
		MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE((size_t)1, (size_t)1);
		_pager_DisplayTopLines();
	}
	FLUSH();
}

void pager_pendingpattern(epager_pending state, char *pattern ) {
#define LABELP	"Pending pattern: "
	size_t origin = vpager_nbCOLS - strlen(LABELP) - strlen(pattern) - 1;
	if (origin <= 0) origin = 1;
	MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE((size_t)1, origin - 1);
	MOVE_CURSOR(1, origin);
	DISPLAY("%s%s%s%s   ", pager_FOREGROUND_3, LABELP, pattern, pager_STOP_COLOR);
	if (state == NCLEAR) {
		MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE((size_t)1, (size_t)1);
		_pager_DisplayTopLines();
	}
	FLUSH();
}

static bool _pager_GetTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		pager_error("%s", "The terminfo database could not be found. Abort!");
		return false;
	case 0:
		pager_error("%s", "There is no entry for this terminal in the terminfo database. Abort!");
		return false;
	default:
		break;
	}
	// Get various string-valued capabilities.
	static char sbuf[TERMSBUF_SIZE];
	char *sp = sbuf;
	if (NULL == (vpager_screeninit = tgetstr("ti", &sp))) {
		pager_error("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database. Abort!");
		return false;
	}
	if (NULL == (vpager_screendeinit = tgetstr("te", &sp))) {
		pager_error("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database. Abort!");
		return false;
	}
	if (NULL == (vpager_clear = tgetstr("cl", &sp))) {
		pager_error("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database. Abort!");
		return false;
	}
	if (NULL == (vpager_move = tgetstr("cm", &sp))) {
		pager_error("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database. Abort!");
		return false;
	}
	//vpager_visual_bell = tgetstr("vb", &sp);
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_DisplayTopLines( void ) {
#define HR 		"<hr>"
#define CENTER 	"<center>"
	if (vpager_headerlines != 0) {
		for (size_t i = 0; i < vpager_headerlines; i++) {
			MOVE_CURSOR(i + 1, 1);
			if (strncmp(vpager_header[i], HR, strlen(HR)) == 0) {
				DISPLAY_HEADERLINE();
				continue;
			}
			if (strncmp(vpager_header[i], CENTER, strlen(CENTER)) == 0) {
				char *s = vpager_header[i] + strlen(CENTER);
				DISPLAY("%s", PREFIX_HEADER);
				for (size_t i = 0; i < (vpager_nbCOLS - strlen(PREFIX_HEADER) - strlen(s)) / 2; i++) DISPLAY(" ");
				DISPLAY("%s", s);
				continue;
			}
			if (vpager_nbCOLS < strlen(vpager_header[i]) + strlen(PREFIX_HEADER)) {
				char *s = strdup(vpager_header[i]);
				s[vpager_nbCOLS - strlen(PREFIX_HEADER)] = '\0';
				DISPLAY("%s%s", PREFIX_HEADER, s);
				free(s);
				continue;
			}
			DISPLAY("%s%s", PREFIX_HEADER, vpager_header[i]);
		}
		MOVE_CURSOR(vpager_headerlines + 1, 1);
		DISPLAY_TOPLINE();
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_DisplayBottomLine( void ) {
	char buf1[256], buf2[128];
	pthread_mutex_lock(&vpager_mutex_screen);
	{
		off_t percent = (_pager_GetPageOffset(vpager_currentpage + 1) * 100) / vpager_filesize;
		switch (vpager_currentmode) {
		case MODE_REGULAR:
			sprintf(buf1, "--- %ld%% --- Page %ld", percent, vpager_currentpage + 1);
			break;
		case MODE_OVERLAP:
			sprintf(buf1, "--- %ld%% --- Page %ld [+/-%d lines]", percent, vpager_currentpage + 1, OVERLAP);
			break;
		}
		if (percent == 100 && strlen(vpager_additional_endbanner) != 0) {
			if (sizeof(buf1) > strlen(buf1) + strlen(vpager_additional_endbanner) + 5) {
				strcat(buf1, " --- ");
				strcat(buf1, vpager_additional_endbanner);
			}
		}
		sprintf(buf2, "%ld chars read out of %ld ", _pager_GetPageOffset(vpager_lastpageRead), vpager_filesize);
		DISPLAY_BOTTOMLINE(buf1, buf2);
		FLUSH();
	}
	pthread_mutex_unlock(&vpager_mutex_screen);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_DisplayMessage( char *message ) {
	pthread_mutex_lock(&vpager_mutex_screen);
	{
		DISPLAY_BOTTOMMESSAGE(message);
		FLUSH();
	}
	pthread_mutex_unlock(&vpager_mutex_screen);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_SetTerminalMode(epager_terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &cooked)) {
			pager_error("%s", "Cannot set the parameter 'MODE_ECHOED' associated with the terminal. Abort!");
			siglongjmp(vpager_env_buffer, EXIT_FAILURE);
		}
		break;
	case MODE_RAW:
		if (0 != tcgetattr(STDIN_FILENO, &cooked)) {
			pager_error("%s", "Cannot set the parameter 'MODE_RAW' associated with the terminal. Abort!");
			siglongjmp(vpager_env_buffer, EXIT_FAILURE);
		}
		raw = cooked;
		//cfmakeraw(&raw);
		raw.c_lflag &= (tcflag_t) ~(ICANON | ECHO);
		raw.c_cc[VMIN] = 1;
		raw.c_cc[VTIME] = 0;
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &raw)) {
			pager_error("%s", "Cannot set the parameter 'MODE_RAW/TCSANOW' associated with the terminal. Abort!");
			siglongjmp(vpager_env_buffer, EXIT_FAILURE);
		}
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_ClearTerminal( size_t start, size_t end ) {
	for (size_t i = start; i <= end; i++) {
		MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(i, (size_t)1);
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static size_t _pager_count_visible_chars( wchar_t *str ) {
	wchar_t *s = str;
	size_t non_printable_nbchars = 0;
	bool escape_ongoing = false;
	while (*s != '\0') {
		if (escape_ongoing) {
			++non_printable_nbchars;
			if (*s == L'm') escape_ongoing = false;
		} else {
			if (*s == L'\033') {
				escape_ongoing = true;
				++non_printable_nbchars;
			}
		}
		++s;
	}
	return wcslen(str) - non_printable_nbchars;
}
static size_t _pager_visible_chars( wchar_t *str, int n ) {
	size_t idx = 0;
	bool escape_ongoing = false;
	while (*str != '\0') {
		if (escape_ongoing) {
			if (*str == L'm') escape_ongoing = false;
		} else {
			if (*str == L'\033')
				escape_ongoing = true;
			else
				--n;
		}
		++idx;
		++str;
		if (n < 0) break;
	}
	return idx;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _pager_ProcessOnePage(epager_page action, size_t pagenumber, size_t startlinenumber, char *pattern ) {
	bool found = false;
	wchar_t buf[BUFSIZE], mbuf[BUFSIZE];
	wchar_t *p, *pfound, *wpattern;
	size_t i, mbslen;

	pthread_mutex_lock(&vpager_mutex_screen);
	{
		switch (action) {
		case TODISPLAY:
		case TOHIGHLIGHT:
			_pager_ClearTerminal(vpager_firstline, vpager_lastline);
			break;
		case TOSEARCH:
			break;
		}
		setlocale(LC_ALL, NULL);
		for (i = 0; i < vpager_nblines; i++) {
			if (fgetws(buf, BUFSIZE, vpager_fd) == NULL) {
				if (feof(vpager_fd)) break;
				pthread_mutex_unlock(&vpager_mutex_screen);
				pager_error("Error when reading file %s. Abort!", vpager_filename);
				siglongjmp(vpager_env_buffer, EXIT_FAILURE);
			}
			if (vpager_currentmode == MODE_REGULAR) {
				if (i == OVERLAP - 1)_pager_SetPageOffsetStart(pagenumber, pager_getoffset(vpager_fd));
				if (i == vpager_nblines - (OVERLAP + 1)) _pager_SetPageOffsetEnd(pagenumber, pager_getoffset(vpager_fd));
			}
			switch (action) {
			case TODISPLAY:
				if ((p = wcschr(buf, L'\n'))) *p = '\0';
				if (vpager_nbCOLS < _pager_count_visible_chars(buf) + strlen(PREFIX_HEADER))
					buf[_pager_visible_chars(buf, (int)(vpager_nbCOLS - strlen(PREFIX_HEADER)))] = L'\0';
				MOVE_CURSOR(vpager_firstline + i, 1);
				DISPLAY("%5ld %s %ls", startlinenumber + i, pager_XSV, buf);
				break;
			case TOSEARCH:
				// Calculate the length required to hold 'patter,' converted to a wide character string
				mbslen = mbstowcs(NULL, pattern, 0);
				wpattern = (wchar_t *)malloc((mbslen + 1) * sizeof(wchar_t));
				(void) mbstowcs(wpattern, pattern, mbslen + 1);
				if (NULL != wcsstr(buf, wpattern)) found = true;
				free(wpattern);
				break;
			case TOHIGHLIGHT:
				if ((p = wcschr(buf, L'\n'))) *p = L'\0';
				p = buf;
				// Calculate the length required to hold 'patter,' converted to a wide character string
				mbslen = mbstowcs(NULL, pattern, 0);
				wpattern = (wchar_t *)malloc((mbslen + 1) * sizeof(wchar_t));
				(void) mbstowcs(wpattern, pattern, mbslen + 1);
				if (NULL != (pfound = wcsstr(p, wpattern))) {
					mbuf[0] = L'\0';
					while (pfound != NULL) {
						*pfound = L'\0';
						wcscat(mbuf, p);
						wcscat(mbuf, pager_FOREGROUND_2_W);
						wcscat(mbuf, wpattern);
						wcscat(mbuf, pager_STOP_COLOR_W);
						p = pfound + wcslen(wpattern);
						pfound = wcsstr(p, wpattern);
					}
					wcscat(mbuf, p);
					p = mbuf;
				}
				free(wpattern);
				if (vpager_nbCOLS < _pager_count_visible_chars(buf) + strlen(PREFIX_HEADER))
					p[_pager_visible_chars(p, (int)(vpager_nbCOLS - strlen(PREFIX_HEADER)))] = L'\0';
				MOVE_CURSOR(vpager_firstline + i, 1);
				DISPLAY("%5ld %s %ls", startlinenumber + i, pager_XSV, p);
				break;
			}
		}
		if (vpager_currentmode == MODE_REGULAR)
			_pager_NewPageAllocation(pagenumber, i);
	}
	pthread_mutex_unlock(&vpager_mutex_screen);
	return found;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_DisplayCurrentPage( void ) {
	vpager_currentmode = MODE_REGULAR;
	pager_setoffset(vpager_fd, _pager_GetPageOffset(vpager_currentpage));
	(void)_pager_ProcessOnePage(TODISPLAY, vpager_currentpage, _pager_GetPageLine(vpager_currentpage), NOPATTERN);
	vpager_state = PAGE_FRESHLY_DISPLAYED;
	_pager_DisplayBottomLine();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_SearchPattern(epager_search action, char *pattern ) {
#define NOTFOUND	"Pattern '%s' not found..."
	bool found = false;
	vpager_currentmode = MODE_REGULAR;
	size_t k = vpager_currentpage;
	while (1) {
		pager_setoffset(vpager_fd, _pager_GetPageOffset(k));
		if ((found = _pager_ProcessOnePage(TOSEARCH, k, _pager_GetPageLine(k), pattern))) break;
		_pager_DisplayBottomLine();
		if (action == BACKWARD) {
			if (k == 0) break;
			--k;
		} else {
			++k;
			if (EOF_OF_FILE(k)) break;
		}
	}
	if (! found) {
		char buf[BUFSIZ];
		sprintf(buf, NOTFOUND, pattern);
		_pager_DisplayMessage(buf);
	} else {
		vpager_currentpage = k;
		pager_setoffset(vpager_fd, _pager_GetPageOffset(k));
		(void) _pager_ProcessOnePage(TOHIGHLIGHT, k, _pager_GetPageLine(k), pattern);
		_pager_DisplayBottomLine();
		vpager_state = PAGE_FRESHLY_DISPLAYED;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_ScrollUp( void ) {
	if (vpager_state == ALREADY_SCROLLED_DOWN) {
		_pager_DisplayCurrentPage();
	} else {
		vpager_currentmode = MODE_OVERLAP;
		pager_setoffset(vpager_fd, _pager_GetPageOffsetStart(vpager_currentpage));
		(void)_pager_ProcessOnePage(TODISPLAY, vpager_currentpage, _pager_GetPageLine(vpager_currentpage) + OVERLAP, NOPATTERN);
		vpager_state = ALREADY_SCROLLED_UP;
		_pager_DisplayBottomLine();
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_ScrollDown( void ) {
	if (vpager_state == ALREADY_SCROLLED_UP) {
		_pager_DisplayCurrentPage();
	} else {
		vpager_currentmode = MODE_OVERLAP;
		pager_setoffset(vpager_fd, _pager_GetPageOffsetEnd(vpager_currentpage - 1));
		(void)_pager_ProcessOnePage(TODISPLAY, vpager_currentpage, _pager_GetPageLine(vpager_currentpage) - OVERLAP, NOPATTERN);
		vpager_state = ALREADY_SCROLLED_DOWN;
		_pager_DisplayBottomLine();
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	CONTEXT MANAGEMENT
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _pager_SetContext( void ) {
	if (! _pager_AllocPageOffsetsTable())						// Allocate the file offet table
		return false;
	vpager_currentpage = vpager_lastpageRead = 0;
	_pager_SetPageOffset(vpager_currentpage, 0);
	_pager_SetPageLine(vpager_currentpage, 1);
	if (! pager_GetTerminalSize(&vpager_nbLINES, &vpager_nbCOLS))	// Get screen size: Number of lines and cols of the screen
		return false;
	if (! _pager_GetTerminalCapabilities())					// Catch terminal capabilities
		return false;
	SAVEINITSCREEN(vpager_nbLINES);							// Save initial state of the screen
	HIDE_CURSOR();											// Hide cursor
	CLEAR_ENTIRE_SCREEN();
	_pager_DisplayTopLines();
	_pager_DisplayBottomLine();
	vpager_firstline = (vpager_headerlines == 0) ? 1 : vpager_headerlines + 2;
	vpager_lastline = vpager_nbLINES - 1;
	vpager_nblines = vpager_lastline - vpager_firstline + 1;
	_pager_SetTerminalMode(MODE_RAW);						// Move terminal in raw mode
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_ResetContext( void ) {
	_pager_SetTerminalMode(MODE_ECHOED);					// Restore terminal mode
	SHOW_CURSOR();											// Restore cursor
	CLEAR_ENTIRE_SCREEN();									// Clear screen
	RESTOREINITSCREEN(vpager_nbLINES);						// Restore initial state of the screen
	_pager_FreePageOffsetsTable();								// Free the file offet table
	if (vpager_fd != NULL) fclose(vpager_fd);				// Close file
	vpager_fd = NULL;
	FLUSH();												// Needed!!!!
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	VIEW FILE
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _pager_View( void ) {
#define REALLOCATION	"More pages than expected. Reallocation of the offset table. Please, wait..."
	// Set screen....
	if (! _pager_SetContext())
		return -1;
	// Go on on first page...
	_pager_DisplayCurrentPage();
	size_t n;
	epager_search lastsearch = NOP;
	char pattern[128] = "";
	for (;;) {
		int c = pager_incoming(&n, pattern, sizeof(pattern));
		_pager_DisplayBottomLine();
		switch (c) {
		case 0:
			break;
		case 1:					// Help
			pthread_mutex_lock(&vpager_mutex_screen);
			{
				pager_help();
				while (0 != pager_incoming(&n, pattern, sizeof(pattern))) {
					DISPLAY_BELL();
				}
			}
			pthread_mutex_unlock(&vpager_mutex_screen);
			_pager_DisplayCurrentPage();
			break;
		case 2:					// Next page
			if (_pager_GetPageOffset(vpager_currentpage + 1) < vpager_filesize) {
				if (vpager_currentpage == vpager_offsetssize - 2) {
					pthread_mutex_lock(&vpager_mutex_screen);
					{
						DISPLAY_MESSAGE((char *)REALLOCATION);
						bool n = _pager_AllocPageOffsetsTable();
					}
					pthread_mutex_unlock(&vpager_mutex_screen);
					if (! n) return -1;
				}
				++vpager_currentpage;
				_pager_DisplayCurrentPage();
			} else {
				DISPLAY_BELL();
			}
			break;
		case 3:					// Previous page
			if (vpager_currentpage > 0) {
				--vpager_currentpage;
				_pager_DisplayCurrentPage();
			} else {
				DISPLAY_BELL();
			}
			break;
		case 4:					// Goto page N
			if (n > 0) {
				if (vpager_lastpageRead < n - 1) {
					if (vpager_lastPageRequested) {
						vpager_currentpage = vpager_lastpageRead - 1;
						_pager_DisplayCurrentPage();
					} else {
						for (vpager_currentpage = vpager_currentpage + 1; vpager_currentpage <= n - 1; vpager_currentpage++) {
							_pager_DisplayCurrentPage();
							if (vpager_lastPageRequested) break;
						}
						if (! vpager_lastPageRequested) --vpager_currentpage;
					}
				} else {
					vpager_currentpage = n - 1;
					_pager_DisplayCurrentPage();
				}
			} else {
				DISPLAY_BELL();
			}
			break;
		case 5:				// 'OVERLAP' next lines
			if ((EOF_OF_FILE(vpager_currentpage + 1) && vpager_state != ALREADY_SCROLLED_DOWN)
			        || vpager_state == ALREADY_SCROLLED_UP) {
				DISPLAY_BELL();
			} else {
				_pager_ScrollUp();
			}
			break;
		case 6:				// 'OVERLAP' previous lines
			if ((vpager_currentpage == 0 && vpager_state != ALREADY_SCROLLED_UP)
			        || vpager_state == ALREADY_SCROLLED_DOWN) {
				DISPLAY_BELL();
			} else {
				_pager_ScrollDown();
			}
			break;
		case 7:				// Refresh
			_pager_DisplayCurrentPage();
			break;
		case 8:				// Goto end
			while (_pager_GetPageOffset(vpager_currentpage + 1) != (vpager_filesize = pager_getsize(vpager_filename))) {
				++vpager_currentpage;
				_pager_DisplayCurrentPage();
			}
			break;
		case 9:					// Information
			pthread_mutex_lock(&vpager_mutex_screen);
			{
				pager_info(vpager_filename);
				while (0 != pager_incoming(&n, pattern, sizeof(pattern))) {
					DISPLAY_BELL();
				}
			}
			pthread_mutex_unlock(&vpager_mutex_screen);
			_pager_DisplayCurrentPage();
			break;
		case 10:			// Search forward
			if (strlen(pattern) == 0) {
				DISPLAY_BELL();
			} else {
				lastsearch = FORWARD;
				_pager_SearchPattern(lastsearch, pattern);
			}
			break;
		case 11:			// Search backward
			if (strlen(pattern) == 0) {
				DISPLAY_BELL();
			} else {
				lastsearch = BACKWARD;
				_pager_SearchPattern(lastsearch, pattern);
			}
			break;
		case 12:			// Repeat previous search
			if (lastsearch == NOP) {
				DISPLAY_BELL();
			} else
				_pager_SearchPattern(lastsearch, pattern);
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (c == 0) break;
	}
	_pager_ResetContext();
	return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	THREADS
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void *_pager_thread( void *arg ) {
	int n = _pager_View();
	pthread_exit((void *)&n);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void *_pager_SizemonitoringThread( void *arg ) {
#define THREADTEMPO		1000	// milliseconds
	while (1) {
		if (vpager_offsets != NULL) {
			off_t s = pager_getsize(vpager_filename);
			if (s != vpager_filesize) {
				vpager_filesize = s;
				if (vpager_lastPageRequested) {
					_pager_DisplayCurrentPage();
					//vpager_lastPageRequested = false;
				} else
					_pager_DisplayBottomLine();
			}
		}
		pager_sleep_ms(THREADTEMPO);
	}
	// Never reached ....
	pthread_exit(NULL);
	return NULL;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SIGNALS
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _pager_sizemanagement( int dummy __attribute__((__unused__)) ) {
	if (vpager_resizing == STOP_ON_RESIZING) {
		siglongjmp(vpager_env_buffer, EXIT_RESIZE);
	}
	if (! pager_GetTerminalSize(&vpager_nbLINES, &vpager_nbCOLS)) {	// Get screen size: Number of lines and cols of the screen
		pager_error("%s", "Cannot fetch terminal size. Abort");
		siglongjmp(vpager_env_buffer, EXIT_FAILURE);
	}
	signal(SIGWINCH, SIG_IGN);
	MEMSET_VPAGER_OFFSET();
	vpager_currentpage = vpager_lastpageRead = 0;
	_pager_SetPageOffset(vpager_currentpage, 0);
	_pager_SetPageOffset(vpager_currentpage + 1, 0);
	_pager_SetPageLine(vpager_currentpage, 1);
	vpager_firstline = (vpager_headerlines == 0) ? 1 : vpager_headerlines + 2;
	vpager_lastline = vpager_nbLINES - 1;
	vpager_nblines = vpager_lastline - vpager_firstline + 1;
	CLEAR_ENTIRE_SCREEN();
	_pager_DisplayTopLines();
	_pager_DisplayBottomLine();
	_pager_DisplayCurrentPage();
	signal(SIGWINCH, _pager_sizemanagement);
}
//------------------------------------------------------------------------------
// MAIN ROUTINES
//------------------------------------------------------------------------------
void pager_cleaner() {
	_pager_ResetContext();
	pthread_cancel(vpager_sizemonitoring);
	pager_freeheader();
}

int pager_view( struct sPager control ) {
	// Initializations
	vpager_filename = control.filename;
	vpager_asynchronousmode = (control.viewtype == ASYNCHRONOUS);
	vpager_resizing = control.resizingtype;
	vpager_prefixerror = control.prefixerror;
	vpager_additional_endbanner = control.endbanner;
	vpager_offsets = NULL;
	vpager_viewthread = 0;
	vpager_sizemonitoring = 0;
	vpager_state = PAGE_FRESHLY_DISPLAYED;
	vpager_currentmode = MODE_REGULAR;
	vpager_headerlines = 0;
	vpager_header = NULL;
	// Consistency checks
	if (! isatty(fileno(stdout)) || ! isatty(fileno(stdin))) {
		pager_error("%s", "stdout or stdin does not refer to a terminal. Abort!");
		return EXIT_FAIL;
	}
	// File checks
	if (! pager_filecheck(vpager_filename))
		return EXIT_FAIL;

	pthread_mutex_init(&vpager_mutex_screen, NULL);
	// Generate the header
	if (! pager_generateheader(control.header)) {
		pager_error("%s", "Cannot allocate memory for header. Abort!");
		return EXIT_FAIL;
	}
	// Open the file
	if (NULL == (vpager_fd = fopen(vpager_filename, "r"))) {
		pager_error("Cannot open file '%s'. Abort!", vpager_filename);
		return EXIT_FAIL;
	}
	// Get file size
	if (0 == (vpager_filesize = pager_getsize(vpager_filename))) {
		pager_error("File '%s' has 0 char. Abort!", vpager_filename);
		return EXIT_FAIL;
	}
	// Launch the size monitoring thread
	pthread_attr_t tattr;
	int ret = pthread_attr_init(&tattr);
	ret = pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);
	pthread_create(&vpager_sizemonitoring, &tattr, _pager_SizemonitoringThread, (void *)"");
	// Initialization of the direct output mechanism
	int n;
	if ((n = sigsetjmp(vpager_env_buffer, 1))) {
		_pager_ResetContext();
		pthread_cancel(vpager_sizemonitoring);
		pager_freeheader();
	} else {
		signal(SIGWINCH, _pager_sizemanagement);
		if (control.viewtype == ASYNCHRONOUS) {
			n = pthread_create(&vpager_viewthread, NULL, _pager_thread, (void *)"");
		} else {
			n = _pager_View();
			pthread_cancel(vpager_sizemonitoring);
			pager_freeheader();
		}
	}
	return n;
}
//------------------------------------------------------------------------------
