# *libPager*, a C library for LINUX to view text files one page at a time.

A C library for LINUX to display a text file on a CRT-like terminal. After each page, a prompt is displayed. The user may then press one of the keys described below:
- **q** or **Q** : Quit
- **h** or **H** : This help
- *enter* or *space* or *▶* :  Next page
- **p** or **P**, or *◀* : Previous page
- *num* + *enter* or *space* or *▶* : Goto page *num*
- *▼* (once only) : Five next lines
- *▲* (once only) : Five previous line
- **r** or **R** : Refresh screen
- **i** or **I** : Information about file
- **/***pattern***/** : Search forward for matching line
- **?***pattern***?** : Search backward for matching line
- **n **or **N** : Repeat previous search


Examples:

- Display:
![libPager](README_images/5.png  "Display")

- On-line help:
![libPager](README_images/2.png  "On-line help")

- File information report:
![libPager](README_images/4.png  "File information report")

## LICENSE
**libPager** is covered by the GNU General Public License (GPL) version 3 and above.

## API DEFINITION
### pager_view
*Synopsys:*

```C
#include "pager.h"
int pager_view( struct sPager p);
```

*Description:*
This function starts the visualization of the text file specified by '*filename*' field of the *sPager-type* p structure.

- The field '*p.header*' points to a string  of characters (ending with '\0') that defines the line or lines that will be displayed at the head of each page. A line is delimited by the character '\n'. A line can start with the keyword '**&lt;center&gt;**'. In this case, the rest of the line will be centered. A line can be reduced to the keyword "**&lt;hr&gt;**". In this case, a horizontal line will be displayed.
Example:
```C
char header[128] = "<center>AUTO-TEST MODE\n<hr>\nCOLUMN 1   COLUMN 2              COLUMN 3                            COLUMN 4";
p.header = header;
```
![libPager](README_images/3.png  "Header")

- The field '*p.endbanner*' points to a text to add to the message displayed at the end of the file.
- The field '*p.prefixerror*' points to the text that will be displayed to start all error messages.
- The field '*p.viewtype*' defines the type of the visualization process:
    - **SYNCHRONOUS**: the visualization process is launched and the current process is suspended till the user quits the visualization.
    - **ASYNCHRONOUS**: The visualization process is started and the current process continues to run. The wait of end of the visualization will be made by the *pager_wait* function.
- The field '*p.resizingmode*' defines the action to do when the window is resized:
    - **REDRAW_ON_RESIZING**: the library will refresh the window and its content by taking into account the new numbers of columns and rows.
    - **STOP_ON_RESIZING**: If the window is resized, the function gives back hand to the caller.Example:

*Return value:*
Upon successful completion *pager_view* return 0. If the window has been resized with the parameter 'STOP_ON_RESIZING', the function return -2. Otherwise, -1 is returned and the error is displayed.

### pager_wait
*Synopsys:*

```C
#include "pager.h"
int pager_wait( void );
```

*Description:*
In *asynchronous* mode, the function suspends execution of the current process until the user has quitted the visualization of the text file. Do not use in synchronous mode.

*Return value:*
Upon successful completion *pager_wait* return 0. Otherwise, -1 is returned and the error is displayed.

### pager_cleaner
*Synopsys:*

```C
#include "pager.h"
void pager_cleaner( void );
```

*Description:*
This function is to be called in the interrupt routine defined by the application. It frees all resources used by the library and resets the terminal.

### pager_version
*Synopsys:*

```C
#include "pager.h"
void pager_version( char *version, size_t length );
```

*Description:*
This function copies the version identification of the **libPager** library, including the terminating null byte ('\0'), to the buffer pointed to by *version*. *length* is the size of this buffer.

## CODE EXAMPLE
```C
#include "pager.h"
...
char datafile[128] = ".libpagerTest.txt";
char header[128];
FILE *fp = NULL;
...
strcpy(header, "<center>AUTO-TEST 2 MODE");
// ... Create data file
if (NULL == (fp = fopen(datafile,"w"))) {
	fprintf(stdout, "AUTO-TEST 2 / ERROR: Cannot create data file.\n");
	return EXIT_FAILURE;
}
// ... Fill it (Part 1)
for (int i = 0; i < 100; i++)
	fprintf(fp, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n\
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n\
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n\
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n\
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n\
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n");
fclose(fp);
// ... View it
control.filename = datafile;
control.header = header;
control.endbanner = (char *) "No more file";
control.prefixerror = (char *) "AUTO-TEST LIBPAGER";
control.viewtype = SYNCHRONOUS;
control.resizingtype = STOP_ON_RESIZING;

k = pager_view(control);
fprintf(stdout, "AUTO-TEST 2: VIEW reports %d.\n", k);

// ... Remove data file
if (0 != remove(datafile)) {
	fprintf(stdout, "AUTO-TEST 2 / ERROR: Unable to delete the file.\n");
	return EXIT_FAILURE;
}
...
```
Resulting display:
![libPager](README_images/5.png  "Resulting display")

## STRUCTURE OF THE APPLICATION
This section walks you through **libPager**'s structure. Once you understand this structure, you will easily find your way around in **libPager**'s code base.
``` bash
$ yaTree
./                      # Application level
├── README_images/      # Images for documentation
│   ├── 1.png           # 
│   ├── 2.png           # 
│   ├── 3.png           # 
│   ├── 4.png           # 
│   └── 5.png           # 
├── src/                # Source directory
│   ├── Makefile        # Makefile
│   ├── pager.h         # Library haeader file
│   ├── pager_data.h    # Internal global header file
│   ├── pager_funcs.c   # Common routines
│   ├── pager_version.c # 'pager_version' function and main of the embedded self-test
│   ├── pager_view.c    # 'pager_view' function
│   └── pager_wait.c    # 'pager_wait' function
├── COPYING.md          # GNU General Public License markdown file
├── LICENSE.md          # License markdown file
├── Makefile            # Makefile
├── README.md           # ReadMe Mark-Down file
├── RELEASENOTES.md     # Release Notes markdown file
└── VERSION             # Version identification text file

2 directories, 18 files

```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE
```Shell
$ cd libPager
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION IN RELEASE MODE
```Shell
$ cd libPager
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## HOW TO BUILD AND RUN THE AUTO-TEST MODE
```Shell
$ cd libPager
$ make pager
$ linux/pager.out
```
## NOTES
- Application woks fine with xterm-like emulators (*xterm*, *uxtem*, *terminator* and *tilix*).
- Application developed and tested with gcc 7.3.0 running on XUBUNTU 18.04.
- Refer to the application **[yaMore]** ((https://gitlab.com/mubunt/yaMore)) built over *libPager*.

## SOFTWARE REQUIREMENTS
- For development and usage: Nothing particular...

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md).
***
