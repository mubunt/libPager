# RELEASE NOTES: *libPager*, a Linux C library to view text files one page at a time.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.2.13**:
  - Updated build system components.

- **Version 1.2.12**:
  - Updated build system.

- **Version 1.2.11**:
  - Removed unused files.

- **Version 1.2.10**:
  - Updated build system component(s)

- **Version 1.2.9**:
  - Renamed executable built as example as *example_xxx* to be compliant with other projects.

- **Version 1.2.8**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.2.7**:
  - Some minor changes in .comment file(s).

- **Version 1.2.6**:
  - Fixed typos in README file.
  - Fixed a bug when looping with "goto end of file" command on file continually updated.
  - Some reworks.

- **Version 1.2.5**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.
  
- **Version 1.2.4**:
  - Updated makefiles and asociated README information.

- **Version 1.2.3**:
  - Fixed bug in *src/Makefile*: forget optimization parameter(s) for generation.

- **Version 1.2.2**:
  - Changed mecchanism to recognize if a file is a text or not. Now, based on "file" command.

- **Version 1.2.1**:
  - Added acknowledgment after error display.

- **Version 1.2.0**:
  - Changed the background colors to be more suitable for the xubuntu 19.10 terminal emulator.
  - Added possibility to add an additional message to concatenate to end banner (100%).
  - Replaced set of 'pager-view' parameters by a structure.
  - Fixed issue in window size management.
  - Changed the interrupt management mecchanism and added new function 'pager_cleaner'.

- **Version 1.1.3**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.1.2:**
  - Added tagging of new release.

- **Version 1.1.1:**
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.1.0:**
    - Displayed UTF-8 Unicode text file with escape sequences.
    - Added 2 windows resizing management modes: local by the library, external by the calling application.
    - Did some cleaning in the use of 'int' vs. 'size_t' types.
    - Fixed a crash cause when the primitives of the library are active repeatedly.

- **Version 1.0.4:**
    - Fixed errors reported by Valgrind.
    - Replaced the RELEASENOTES text file by a Mark-Down one.

- **Version 1.0.3:**
    - Fixed typos in README.md file.

- **Version 1.0.2:**
    - Fixed core dump after double call to the view function.
    - Improved the embedded test program.

- **Version 1.0.1:**
    - Replaced "right/left/up/down arrows" by geometric shapes ▶, ◀, ▲ and ▼

- **Version 1.0.0:**
    - First release.
___
