//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libPager
// C library to view text files one page at a time.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "pager.h"
//------------------------------------------------------------------------------
// TEST
//------------------------------------------------------------------------------
int main( void ) {
	// Identification
	char version[128];
	pager_version(version, sizeof(version));
	fprintf(stdout, "%s\n\n", version);
	//--------------------------------------------------------------------------
	struct sPager control;
	//--------------------------------------------------------------------------
	char datafile[128] = ".libpagerTest.txt";
	int k;
	// Tests 1
	char header[128] = "<center>AUTO-TEST 1 MODE\n<hr>\nCOLUMN 1   COLUMN 2              COLUMN 3                            COLUMN 4";
	FILE *fp = NULL;
	// ... Create data file
	if (NULL == (fp = fopen(datafile,"w"))) {
		fprintf(stdout, "AUTO-TEST 1 / ERROR: Cannot create data file.\n");
		return EXIT_FAILURE;
	}
	fprintf(stdout, "AUTO-TEST 1: File created successfully.\n");
	// ... Fill it (Part 1)
	for (int i = 1; i < 1001; i++)
		fprintf(fp, "Line %05d AAAAAAAAAAAAAAAAAAAAB BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC CCCCCCCCCCCCCCCCCCCCCD\n", i);
	fflush(fp);
	fprintf(stdout, "AUTO-TEST 1: File written successfully.\n");
	// ... View it
	control.filename = datafile;
	control.header = header;
	control.endbanner = (char *) "";
	control.prefixerror = (char *) "AUTO-TEST LIBPAGER: ";
	control.viewtype = ASYNCHRONOUS;
	control.resizingtype = REDRAW_ON_RESIZING;
	k = pager_view(control);
	fprintf(stdout, "AUTO-TEST 1: VIEW reports %d.\n", k);
	// ... Fill it (Part 2)
	sleep(10);
	for (int i = 1001; i < 1301; i++)
		fprintf(fp, "Line %05d AAAAAAAAAAAAAAAAAAAAA BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC CCCCCCCCCCCCCCCCCCCCCD\n", i);
	fclose(fp);
	// ... Waiting for the goodwill of the user
	k = pager_wait();
	fprintf(stdout, "AUTO-TEST 1: WAIT reports %d.\n", k);

	// ... Remove data file
	if (0 != remove(datafile)) {
		fprintf(stdout, "AUTO-TEST 1 / ERROR: Unable to delete the file.\n");
		return EXIT_FAILURE;
	}
	fprintf(stdout, "AUTO-TEST 1: File deleted successfully.\n");
	//--------------------------------------------------------------------------
	// Tests 2
	strcpy(header, "<center>AUTO-TEST 2 MODE");
	// ... Create data file
	if (NULL == (fp = fopen(datafile,"w"))) {
		fprintf(stdout, "AUTO-TEST 2 / ERROR: Cannot create data file.\n");
		return EXIT_FAILURE;
	}
	fprintf(stdout, "AUTO-TEST 2: File created successfully.\n");
	// ... Fill it (Part 1)
	for (int i = 0; i < 100; i++)
		fprintf(fp, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n\
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n\
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n\
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n\
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n\
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n");
	fprintf(stdout, "AUTO-TEST 2: File written successfully.\n");
	fclose(fp);
	// ... View it
	control.filename = datafile;
	control.header = header;
	control.endbanner = (char *) "No more file";
	control.prefixerror = (char *) "AUTO-TEST LIBPAGER";
	control.viewtype = SYNCHRONOUS;
	control.resizingtype = STOP_ON_RESIZING;
	//do {
	k = pager_view(control);
	fprintf(stdout, "AUTO-TEST 2: VIEW reports %d.\n", k);
	//} while (k == -2);

	// ... Remove data file
	if (0 != remove(datafile)) {
		fprintf(stdout, "AUTO-TEST 2 / ERROR: Unable to delete the file.\n");
		return EXIT_FAILURE;
	}
	fprintf(stdout, "AUTO-TEST 2: File deleted successfully.\n");
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
